function deploy {
    (
        echo "Changing directory to $1"
        cd $1;\
        echo "Deploying application to stage $3 ...";
        sls deploy --stage $3;\
    )
}

deploy . $1 $2