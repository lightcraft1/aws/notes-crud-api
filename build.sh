function build {
        echo "Building project ..."
        echo $1
        cd $1;\
        echo "Building $1 to stage $2";
        sls package --package $2 --stage $3
}

build . $1 $2